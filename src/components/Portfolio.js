import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import project1 from '../images/indecision.jpg';
import project2 from '../images/tree.jpg';
import project3 from '../images/akita.jpg';
import {
  Box,
  Grid,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles({
  mainContainer: {
    background: '#233',
    height: '100'
  },
  cardContainer: {
    maxWidth: 345,
    margin: '3rem',
    margin: '5rem auto'
  }
});

const Portfolio = () => {
  const classes = useStyles()
  return (
    <Box
      component='div'
      className={classes.mainContainer}
    >
      <Grid
        container
        justify='center'
        alignItems='center'
      >
        <Grid
          item xs={12}
          sm={8}
          md={6}
        >
          <Card className={classes.cardContainer}>
            <CardActionArea>
              <CardMedia
                component='img'
                alt='Project 1'
                height='140'
                image={project1}
              />
              <CardContent>
                <Typography gutterBottom variant='h5'>
                  Indecision App
                </Typography>
                <Typography
                  variant='body2'
                  color='textSecondary'
                  component='p'
                >
                  App that decides what to do.
                </Typography>
              </CardContent>
              <CardActions>
                <Button
                  size='small'
                  color='primary'>
                  <a href='https://www.linkedin.com/in/%C5%82ukasz-p%C5%82ocieniak-b01ba61a9/'>Link</a>
                  </Button>
              </CardActions>
            </CardActionArea>
          </Card>
          <Card className={classes.cardContainer}>
            <CardActionArea>
              <CardMedia
                component='img'
                alt='Project 2'
                height='140'
                image={project2}
              />
              <CardContent>
                <Typography gutterBottom variant='h5'>
                  Tree webpage
              </Typography>
                <Typography
                  variant='body2'
                  color='textSecondary'
                  component='p'
                >
                  Website about protecting trees and environment
              </Typography>
              </CardContent>
              <CardActions>
                <Button
                  size='small'
                  color='primary'>
                  <a href='https://www.linkedin.com/in/%C5%82ukasz-p%C5%82ocieniak-b01ba61a9/'>Link</a>
                </Button>
              </CardActions>
            </CardActionArea>
          </Card>
          <Card className={classes.cardContainer}>
            <CardActionArea>
              <CardMedia
                component='img'
                alt='Project 3'
                height='140'
                image={project3}
              />
              <CardContent>
                <Typography gutterBottom variant='h5'>
                  Akita webpage
            </Typography>
                <Typography
                  variant='body2'
                  color='textSecondary'
                  component='p'>
                  Webpage about Akita Inu
            </Typography>
              </CardContent>
              <CardActions>
                <Button
                  size='small'
                  color='primary'>
                  <a href='https://www.linkedin.com/in/%C5%82ukasz-p%C5%82ocieniak-b01ba61a9/'>Link</a>
                </Button>
              </CardActions>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    </Box>
  )
}

export default Portfolio;